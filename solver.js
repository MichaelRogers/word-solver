/*function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
}*/


// This array will hold all of the words we generate from our letter list.
const validWords = [];

const Combinatorics = require('js-combinatorics');
const fs = require('fs');
let desiredSize = null;
let positionString = "";

// Was a specified word size passed in?
if(process.argv[3] != null){
    desiredSize = process.argv[3];
}

/*/ We now need to handle position. 
if(process.argv[4] != null){
    positionString = process.argv[4];
    console.log(positionString);
    console.log(positionString);
    // Let's look at each character in the position string
    for(var i=0; i <= positionString.length; i++ ){
        if(isLetter(positionString[i])){
            console.log(true);
        }
    }

}
*/



// We need to grab our argument which will be a string of the letters that we are working with.
if(process.argv[2] != null){
    let letters = process.argv[2];

    // We need to now check to see if the size of our string is two or less.
    if(letters.length <= 2){
        // It was two or less. Return an error
        console.error("Not enough letters provided.");
        return;
    }

    lettersArray = letters.split('');
    
    // We have enough letters, let's break them into an array.
    console.log(lettersArray); 

    // We need to load our dicitionary.
    fs.readFile(__dirname + '/words_dictionary.json', (err, data) => {  
        if(err){
            throw err;
        }

        dictionary = JSON.parse(data);

        //console.log(dictionary);

        // Now let's call the function to handle our combinations.
        let combinations = Combinatorics.permutationCombination(lettersArray);
        let word ="";
        //console.log(cmb.toArray());
        combinations.forEach(function(element) {
            word = element.join('');
            if(dictionary.hasOwnProperty(word) && word.length >=3 && !validWords.includes(word)){
                if(desiredSize != null && desiredSize == word.length){
                    validWords.push(word);
                }
                else if(desiredSize == null){
                    validWords.push(word);
                }
                //console.log(word);
            }
        });
        console.log(validWords);
    });   
    // Let's now get a list of all possible combinations.
    //generateCombos(lettersArray);
}
else{
    console.error('No letters provided');
    return;
}